/*jshint esversion: 6 */
//to set package management, npm init --yes 

const express = require('express'); //to install, on cmd: npm i express
const app = express();
const Joi = require('joi'); //to install joi, on cmd: npm i joi@13.1.0
app.use(express.json());

const courses = [
    { id:1 , name:'course1' },
    { id:2 , name:'course2' },
    { id:3 , name:'course3' }
];

app.get('/', (req,res) => {
    res.send('RJ Sent - HW');
    console.log('New connection...');
});

app.get('/api/courses', (req,res) => {
    res.send(courses);
});

app.get('/api/courses/:id', (req,res) => {
    //res.send(req.query);
    let course = courses.find( c => c.id === parseInt(req.params.id));
    if(!course) return res.status(404).send(`The course with the given ID[${req.params.id}] was not found.`);
    res.send(course);
});

app.post('/api/courses', (req,res) => {
    //Validate, if invalid - return 400 Bad request
    const { error } = ValidateCourse(req.body); //Object destructuring
    if(error) return res.status(400).send(error.details[0].message);

    let course = {
        id: courses.length + 1,
        name: req.body.name
    };
    courses.push(course);
    res.send(course);
});

app.put('/api/courses/:id', (req,res) => {

    //Look up course, if not exist - return 404 Not found
    let course = courses.find( c => c.id === parseInt(req.params.id));
    if(!course) return res.status(404).send(`The course with the given ID[${req.params.id}] was not found.`);

    //Validate, if invalid - return 400 Bad request
    const { error } = ValidateCourse(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    //Update course - return the updated course
    course.name = req.body.name;
    res.send(course);
});

app.delete('/api/courses/:id', (req,res) => {
    let course = courses.find( c => c.id === parseInt(req.params.id));
    if(!course) return res.status(404).send(`The course with the given ID[${req.params.id}] was not found.`);

    const index = courses.indexOf(course);
    courses.splice(index,1);

    res.send(course);
});

function ValidateCourse(course){
    const schema = {
        name : Joi.string().min(3).required()
    };
    return Joi.validate(course, schema);
}

//export PORT = 5000, TO set enviroment variable cmd
const port = process.env.PORT || 3000;
app.listen(port,() => console.log(`Listening on port ${port}...`));