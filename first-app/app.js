/** Export/get from another doc **/
// const log = require('./logger');
// log('mensaje');

/** Path module **/
// const path = require('path');
// var pathObj = path.parse(__filename);
// console.log(pathObj);

/** OS module **/
// const os = require('os');
// var totalMemory = os.totalmem();
// var freeMemory = os.freemem();
// console.log(`Total Memory: ${totalMemory}`);
// console.log(`Free Memory: ${freeMemory}`);

/** fileSystem module **/
// const fs = require('fs');
// //SYNCHRONOUS METHOD
// // const files = fs.readdirSync('./');
// // console.log(files);

// //ASYNCHRONOUS METHOD
// fs.readdir('./',function(err,files){
//     if(err) console.log('Error',err);
//     else console.log(files);
// });

// // /** Event (simple) emit/on class module **/
// const EventEmitter = require('events');
// const emitter = new EventEmitter();

// // Register a listener
// emitter.on('messageLogged',(arg) => {
//     console.log('Listener called',arg);
// });

// // Raise an event
// emitter.emit('messageLogged',{id: 1, url: 'http://' });

// // /** Event (usando una clase) emit/on class module **/
// const Logger = require('./logger');
// const loggerObj = new Logger();

// // Register a listener
// loggerObj.on('messageLogged',(arg) => {
//     console.log('Listener called',arg);
// });

// loggerObj.log('new message');


/** Build Server basic example - http class module **/
const http = require('http');
const server = http.createServer((req,res) => {
    if(req.url === '/'){
        res.write('Hello world RJ');
        res.end();
    }

    if(req.url === '/api/courses'){
        res.write(JSON.stringify([1, 2, 3]));
        res.end();
    }

});

server.listen(3000);
console.log('Listening to port 3000...');

