%{

%}

%locations 
%define api.pure full 

/* operator associations and precedence */

%left '+' '-'
%left '*' '/'
%left '^'
%right '!'
%right '%'
%left UMINUS

%start INICIO

%% /* language grammar */

INICIO: LISTA EOF ;

LISTA: LISTA OPA tk_ptcoma
        {
            console.log($2);
        }
    | OPA tk_ptcoma
        {
            console.log($1);
        }
    | error tk_ptcoma
    ;

OPA: OPA '+' OPA { $$ = parseInt($1) +parseInt($3); }
    | OPA '-' OPA { $$ = $1-$3; }
    | OPA '*' OPA { $$ = $1*$3; }
    | OPA '/' OPA
        {
            $$ = $1/$3;
        //   console.log("Posicion valor 3: " +
        //             @3.first_line + ", " + 
        //             @3.first_column + ", " + 
        //             @3.last_line + ", " + 
        //             @3.last_column + ", " + 
        //             ".");
        }
    | OPA '^' OPA { $$ = Math.pow($1, $3); }
    | OPA '!'
        {{
          $$ = (function fact (n) { return n==0 ? 1 : fact(n-1) * n })($1);
        }}
    | OPA '%' { $$ = $1/100; }
    | '-' OPA %prec UMINUS { $$ = -$2; }
    | '(' OPA ')' { $$ = $2; }
    | Numero { $$ = $1; }
    | E { $$ = Math.E; }
    | PI { $$ = Math.PI; }
    ;
